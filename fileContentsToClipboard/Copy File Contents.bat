@echo off
rem Only works on computers with GDI+ installed (in Windows XP or newer it is installed by default).
set type=readfile
IF /I %~x1 ==.BMP set type=copyimage
IF /I %~x1 ==.GIF set type=copyimage
IF /I %~x1 ==.JPG set type=copyimage
IF /I %~x1 ==.JPEG set type=copyimage
IF /I %~x1 ==.PNG set type=copyimage
IF /I %~x1 ==.TIFF set type=copyimage
%~dp0nircmdc.exe clipboard %type% %~f1
Exit