# Total Commander  
For anything related to Total Commander (https://www.ghisler.com/).  

[[_TOC_]]

## Windows Integration Script  
Integrates Total Commander to Windows by replacing Windows Explorer or just adding an option to Explorer's context menu (shell integration):  

Source: https://gitlab.com/pplupo/total-commander/-/tree/master/windowsIntegration  

Download: https://gitlab.com/pplupo/total-commander/-/raw/master/windowsIntegration/TCMWindowsIntegration.bat?inline=false  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/windowsIntegration/tcmdwindowsintegration.png?inline=false)  

## Command-line compression utilities  
Command-line compression utilities can be configured as external packers so that Total Commander can compress in other formats. Currently, Total Commander can compress in ARJ, LHZ, RAR, UC2, and ACE provided that these external packers are available. Please note that UC2 is disabled in Total Commander when running on 64-bit Windows. The last known version to have it enabled is [Total Commander 7.55](https://totalcommander.ch/win/old/tcmd755.exe).  

Here are all of them in a single file:  
Download: https://gitlab.com/pplupo/total-commander/-/raw/master/packers/packers.7z?inline=false  

All external packers are 32-bit ONLY except RAR, which is provided in 32-bit and 64-bit.  
The latest LHA version that works on Windows 10 64-bit that I could find is 2.67.00 (unfortunately, I could only find it in Japanese). Files compressed with 2.67.00 are known not to be compatible with versions older than 2.13, which is why this older version is also available in the package.  

Licenses:  
* LHA is free and was obtained here: https://www.vector.co.jp/vpack/browse/person/an000224.html  
* ARJ is free and was obtained here: http://arj.sourceforge.net/  
* UC2 is free and can be obtained from different links provided by it's author: https://www.nicodevries.com/professional/
* RAR and ACE are trial/shareware. They are free to distribute, but if you want to keep using them, you must register.  

If you want to pick just the one(s) you are interested in, the separate downloads are available here: https://gitlab.com/pplupo/total-commander/-/tree/master/packers  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/packers/screenshot.png?inline=false)  

## Buttons  
This is a list of useful buttons for Total Commander. Some of these require specific parameters, external tools, or scripts to work. Please follow the configuration steps carefully.  

Be aware that in all examples, the location of the scripts or tools is %COMMANDER_PATH%\addons. If you place them in a different directory, you will have to adapt it.  

### Run as Admin  
This button allows running the selected file elevated (as the Administrator user).  

It uses NirCmd elevate command. Therefore it requires NirCmd to be present.  

[NirCmd](https://www.nirsoft.net/utils/nircmd.html) is a small free command-line utility that can be downloaded directly from the original website (recommended, for the up-to-date version):  

* [32-bit](https://www.nirsoft.net/utils/nircmd.zip)  
* [64-bit](https://www.nirsoft.net/utils/nircmd-x64.zip)  

It is also available here for archiving purposes:  

* [32-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/nirCmd/nircmd.zip)  
* [64-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/nirCmd/nircmd-x64.zip)  

This is how to set up the button:  
* Command: %COMMANDER_PATH%\addons\NirCmd\nircmdc.exe elevate  
* Parameters: %P%N  

Optionally, you can set up the icon of your choice since NirCmd doesn't provide any. Two suggestions are:  

* %COMMANDER_PATH%\TCMADM64.EXE (Total Commander Administrator tool).  
* %systemroot%\system32\imageres.dll, icon 73 (yellow and blue shield, Windows 10 default for admin, as in the screenshot below).  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/runAsAdmin/screenshot.png)  

### Copy file contents to the clipboard  
For image files, it transfers the image to the clipboard. For text files (txt, log, bat, etc.), it transfers the text. Image formats supported are bmp, gif, jpg/jpeg, png, and tiff.  

This only works on computers with GDI+ installed (in Windows XP or newer it is installed by default). So for Windows versions newer than XP, this will work out of the box.  

For older Windows versions, GDI+ is hard to find. I found the latest version here: [Download Platform SDK Redistributable: GDI+ from Official Microsoft Download Center](https://web.archive.org/web/20160303183703/http://www.microsoft.com/en-us/download/details.aspx?id=18909). You can download it from there and try it at your own risk. I am also providing the same file downloaded from there, here, for convenience: [GDI+](https://gitlab.com/pplupo/total-commander/-/raw/master/fileContentsToClipboard/WindowsXP-KB975337-x86-ENU.exe)  

I have also found some references saying that it could be part of .NET framework, so I would rather try that first and see if it works.  

It is based on NirCmd features that copy contents of different file types to the clipboard. I'm providing a script to make the parameters independent of the file type. So, download the script here: [batch script] (https://gitlab.com/pplupo/total-commander/-/raw/master/fileContentsToClipboard/Copy%20File%20Contents.bat?inline=false), save it on the same directory as NirCmd.  

[NirCmd](https://www.nirsoft.net/utils/nircmd.html) is a small free command-line utility that can be downloaded directly from the original website (recommended, for the up-to-date version):  

* [32-bit](https://www.nirsoft.net/utils/nircmd.zip)  
* [64-bit](https://www.nirsoft.net/utils/nircmd-x64.zip)  

It is also available here for archiving purposes:  

* [32-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/nirCmd/nircmd.zip?inline=false)  
* [64-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/nirCmd/nircmd-x64.zip?inline=false)  

This is how to set up the button:  
* Command: "%COMMANDER_PATH%\addons\NirCmd\Copy File Contents.bat"  
* Parameters: %S  
* Run minimized: check  

Optionally, you can set up the icon of your choice since NirCmd doesn't provide any. I suggest C:\WINDOWS\System32\imageres.dll, icon 241 as in the screenshot below.  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/fileContentsToClipboard/screenshot.png)  

### Open bash in current dir.  
This script uses a portable version of Cygwin when Windows Subsystem for Linux is not available to provide Linux bash emulation in Windows environments. It will open bash in the current directory of the target panel.  

The script needs to be downloaded from here: [batch script](https://gitlab.com/pplupo/total-commander/-/raw/master/bashInCurrentDir/LinuxShell.bat?inline=false) and will work only if the Windows Subsystem for Linux is installed or if Cygwin portable is available in the same directory. It was designed to work with Cygwin Portable that is installed using this script: [Portable Cygwin](https://sourceforge.net/projects/cygwin-portable/).  

For archiving purposes, it can be downloaded here: [Cygwin portable archive]  (https://gitlab.com/pplupo/total-commander/-/raw/master/bashInCurrentDir/cygwin-portable.zip?inline=false).  

This is how to set up the button:  

* Command: %COMMANDER_PATH%\addons\Cygwin\LinuxShell.bat  
* Parameters: "%P"  
* Start path: %COMMANDER_PATH%\addons\Cygwin\  
* Run minimized: check  

You can choose to adopt Cygwin's icon (go to the directory where it was downloaded and go to <download_dir>\App\cygwin\Cygwin.ico or any icon of your preference. In the screenshot below, a Tux icon is being used. It was downloaded from [IconArchive](https://iconarchive.com/show/operating-systems-icons-by-tatice/Linux-icon.html) and available here for convenience: [Tux icon](https://gitlab.com/pplupo/total-commander/-/raw/master/bashInCurrentDir/icon.ico).  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/bashInCurrentDir/screenshot.png)  

### Open Powershell in current dir.  
OK, this one is no big deal. You have Powershell already installed. My effort was just determining the parameters and finding the location with environment variables so that it works anywhere.  

This is how to set up the button:  

* Command: %WINDIR%\system32\WindowsPowerShell\v1.0\powershell.exe  
* Parameters: -NoExit -Command Set-Location -LiteralPath '%P'  
* Icon: %WINDIR%\system32\WindowsPowerShell\v1.0\powershell.exe  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/powerShellInCurrentDir/screenshot.png)  

### Unlock File/Directory  
This uses [LockHunter](https://lockhunter.com) to allow unlocking files and directories that may be locked by other processes. LockHunter tells you the process that is locking it and it can "remove the lock", delete the file, and/or terminate the process that is locking it. For the most updated version, it is advisable to download the portable version from the official website: [LockHunter Download](https://lockhunter.com/download.htm). Note that the portable version is a link below the big download button. For archiving and convenience, there is a version available here: [Download archived version](https://gitlab.com/pplupo/total-commander/-/raw/master/unlockFileDir/lockhuntersetup_portable_3-4-2.exe?inline=false).  

This is how to set up the button:

* Command: %COMMANDER_PATH%\addons\LockHunter\LockHunter64.exe  
* Parameters: %P%S
* Icon: %COMMANDER_PATH%\addons\LockHunter\LockHunter64.exe  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/unlockFileDir/screenshot.png)  

### WinMerge  
[WinMerge](https://winmerge.org/) is one of the best solutions to merge files, and it is free and open-source. You can download the latest portable versions at [Winmerge Downloads] (https://winmerge.org/downloads/?lang=en), but it is also offered here for the sake of convenience (and archiving):  

* [32-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/winMerge/winmerge-2.16.12-exe.zip?inline=false)  
* [64-bit](https://gitlab.com/pplupo/total-commander/-/raw/master/winMerge/winmerge-2.16.12-x64-exe.zip?inline=false)  

It will compare up to three files or three directories.  

This is how to set up the button:  

* Command: %COMMANDER_PATH%\addons\WinMerge\WinMergeU.exe  
* Parameters: %S %T%M  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/winMerge/screenshot.png)  

### VeraCrypt mount/unmount  
[VeraCrypt](https://veracrypt.fr) is a free, open-source disk encryption software. Updated portable versions can be downloaded at the [official download site](https://veracrypt.fr/en/Downloads.html), and I am also archiving them here:  

* [Windows XP, Vista, 7](https://gitlab.com/pplupo/total-commander/-/raw/master/VeraCryptMountUnmount/VeraCrypt/Legacy%20Portable%201.24-Update7.exe?inline=false)  
* [Windows 8 or newer]https://gitlab.com/pplupo/total-commander/-/raw/master/VeraCryptMountUnmount/VeraCrypt/Portable%201.24-Update7.exe?inline=false)  

VeraCrypt can be used to mount and unmount encrypted files as virtual drives. The buttons below allow you to automatically mount the target file or dismount the selected unit (or all units if none is selected).  

Mount  
This is how to set up the button:  
* Command: %COMMANDER_PATH%\addons\VeraCrypt\VeraCrypt.exe  
* Parameters: /a /q /v %S  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/VeraCryptMountUnmount/screenshotMount.png)  

Unmount  
This is how to set up the button:  

* Command: %COMMANDER_PATH%\addons\VeraCrypt\VeraCrypt.exe  
* Parameters: /q /f /d %s  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/VeraCryptMountUnmount/screenshotUnmount.png)  

### Undelete with Clever File's Disk Drill  
Clever File's [Disk Drill](https://www.cleverfiles.com/data-recovery-software.html) is possibly the best and most complete undeletion tool out there. The integration does nothing more than simply opening the software. I'd like to come up with a way to open it pointing to a directory where I would like the deleted entries (files and subdirectories) to be recovered from, but I could not find any reference to parameters to do so. It is still essential to have such a tool present for when you need it. If you delete a file and download such s tool, you are already at risk of saving it over the file you would like to recover.  

Disk Drill doesn't provide a portable version. However it is already pretty portable by itself. You can download it at the [official site](https://www.cleverfiles.com/download.html), install it, make a copy of the installation directory, and uninstall it. There you go, you have a portable version. I'm providing my portable version here for [download](https://gitlab.com/pplupo/total-commander/-/raw/master/undeleteDiskDrill/CFDiskDrill.zip?inline=false) as well as the installer for archiving purposes: [Installer](https://gitlab.com/pplupo/total-commander/-/raw/master/undeleteDiskDrill/disk-drill-win.exe?inline=false)  

This is how to set up the button:  

* Command: "%COMMANDER_PATH%\addons\CleverFiles\Disk Drill\DD.exe"  

In the screenshot below, you will note that I've used a more intuitive icon:  

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/undeleteDiskDrill/screenshot.png)  

The icon was downloaded from [Icon Library](https://icon-library.net/icon/undelete-icon-7.html) and is provided here for convenience: [Undelete icon](https://gitlab.com/pplupo/total-commander/-/raw/master/undeleteDiskDrill/icon.ico)  

### Download torrents with qBittorrent  
[qBittorrent](https://www.qbittorrent.org/) is a small, free, open-source BitTorrent client. This button will allow you to select a torrent file and download it without having to actually open the application or click on anything. By default, it will download the torrent to a folder with the same name of the torrent file at the same location (make sure you have permission to write there). If you want to move, rename or delete the content you downloaded, be aware that it may be locked for a few seconds while the download finishes and the program closes.  

qBittorrent supports running as a portable application, following the instructions here: [qBittorrent portable](https://github.com/qbittorrent/qBittorrent/wiki/How-to-use-portable-mode#windows). Note that they ask you to keep a file called "qbittorrent.pdb" that is hunderds of megabytes large. You don't need it. It's used only by developers to identify issues and fix qBittorrent. [It was made optional in December 2020](https://github.com/c0re100/qBittorrent-Enhanced-Edition/issues/201).

However, even if you do that, several steps listed below need to be taken in order to set it up for a "transparent" download. For convenience, I'm providing a pre-configured portable qBittorrent (x64) for download here: [qBittorrent ready to go](https://gitlab.com/pplupo/total-commander/-/raw/master/qBittorrent/qBittorrent.7z?inline=false)  

After downloading it, the only thing you need to do is to set up the button like this:

* Command: "%COMMANDER_PATH%\addons\qBittorrent\torrent.bat"  
* Parameters: %P%N
* Run minimized: check  
* Icon file: "%COMMANDER_PATH%\addons\qBittorrent\qbittorrent.exe

Screenshot: ![Screenshot](https://gitlab.com/pplupo/total-commander/-/raw/master/qBittorrent/screenshot.png)  

If you want to download the official version and set it up yourself, here is what you need to do:  

On the menu Tools, select Options.  
As in the picture below:  
* **Check**  
    * Start qBittorrent minimized  
    * Show qBittorrent in notification area  
    * Minimize qBittorrent to notification area  
* **Uncheck**  
    * Check for program updates  
    _Here is an issue: you can choose to keep this checked. However, sometimes when you want to download a torrent, it will nag you with a dialog to update. To do so, you must download the installer and overwrite "qbittorrent.exe" in your qBittorrent folder._  
![qBittorrent options](https://gitlab.com/pplupo/total-commander/-/raw/master/qBittorrent/qBittorrentOptions.png)  

One last setting:  
* Tools > On Downloads Done > Exit qBittorrent  
![qBittorrent exit after download](https://gitlab.com/pplupo/total-commander/-/raw/master/qBittorrent/qBittorrentExit.png)  
This will make qBittorrent quit and unlock the file, as well as its internal copy of the tracker, which is removed by the script.  

This script must be copied to the qbittorrent portable, as this is what we will use to start the download and remove the tracker after qBittorrent closes:  
[Script download](https://gitlab.com/pplupo/total-commander/-/raw/master/qBittorrent/torrent.bat?inline=false)  

Then you just need to create the button like the instructions above.

### Total Commander Send/Receive  
This one uses a combination of tar pipes and netcat to send files or directories (up to 9 items selected in the same directory) over the network to another Total Commander PC.

Just download [sendReceive.7z](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/sendReceive.7z?inline=false), extract it, and create the buttons using the send.bat and receive.bat files as explained below.  

Sending screenshot: ![Screenshot Send Window](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/sendScreenshot.png)  
Receiving screenshot: ![Screenshot Receive Window](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/receiveScreenshot.png)  

* Send button (screenshot below):  
	* Command: %COMMANDER_PATH%\addons\sendReceive\send.bat  
	* Parameters: %S  
	* Icon file: %COMMANDER_PATH%\addons\sendReceive\icons.icl  
Screenshot: ![Screenshot Send Button Config](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/sendButtonConfig.png)  

* Receive button (screenshot below):  
	* Command: %COMMANDER_PATH%\addons\sendReceive\receive.bat  
	* Parameters: %P  
	* Icon file: %COMMANDER_PATH%\addons\sendReceive\icons.icl  
Screenshot: ![Screenshot Receive Button Config](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/receiveButtonConfig.png)  

By default, the sender will request the receiver's IP and won't allow any other IP to receive the content. If you want to change this behavior, open the "config.txt" file and change  
requestReceiverIp=true  
to  
requestReceiverIp=false  
After this change, the sender won't request the receiver's IP anymore. It will accept requests from any IP.  

In the same "config.txt" file, you can also change the default port (it needs to be the same in the sender and the receiver):  
port=1981  

This was built using:  
* [GNU Tar for Windows](http://gnuwin32.sourceforge.net/packages/gtar.htm)  
* [Nmap: the Network Mapper - Free Security Scanner](https://nmap.org/download.html)  
* [Font Awesome](https://fontawesome.com/) icons acquired here:  
	* [https://www.iconfinder.com/icons/1608710/upload_icon](https://www.iconfinder.com/icons/1608710/upload_icon)  
	* [https://www.iconfinder.com/icons/1608668/download_icon](https://www.iconfinder.com/icons/1608668/download_icon)  
To update these binaries, visit the GNU Tar for Windows and Nmap pages, download the new binaries, and overwrite the existing ones.  

_For Windows versions 95 / 98 / ME / NT / 2000 / XP / 2003, GNU Tar has dependencies on 2 Windows DLLs that are not in the system by default. Unfortunately, the Microsoft DLL's link on the page is broken. I am providing the ones I got from my Windows installation (Windows 10 Pro, version 20H2 build 19042.985) here: [Microsoft DLLs](https://gitlab.com/pplupo/total-commander/-/raw/master/sendReceive/MSDependencies.7z?inline=false)  
They were found in C:\Windows\System32\._

#### Linux
If you want to send or receive content between TC and a Linux machine, these commands will likely work. Replace the brackets with the corresponding values:  
* Receive: ncat [sender_ip] [port] | tar -xf -  
	* E.g.: ncat 192.168.1.11 1981 | tar -xf -  
* Send files: tar -cf - [files_list] | ncat -l --send-only -p [port] --allow [receiver_ip]  
	* E.g.: tar -cf - file1.txt file2.png | ncat -l --send-only -p 1981 --allow 192.168.1.11  
* Send directory: tar -cf - [directory] | ncat -l --send-only -p [port] --allow [receiver_ip]  
	* E.g.: tar -cf - /home/username/example | ncat -l --send-only -p 1981 --allow 192.168.1.11  