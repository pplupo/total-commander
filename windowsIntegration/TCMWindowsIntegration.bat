@echo off

REM Get Windows Version
for /f "tokens=2 delims=[]" %%i in ('ver') do set VERSION=%%i
for /f "tokens=2-3 delims=. " %%i in ("%VERSION%") do set VERSION=%%i.%%j
if "%VERSION%" == "1.01" goto :unsupportedWindowsVersion
if "%VERSION%" == "1.02" goto :unsupportedWindowsVersion
if "%VERSION%" == "1.03" goto :unsupportedWindowsVersion
if "%VERSION%" == "1.04" goto :unsupportedWindowsVersion
if "%VERSION%" == "2.03" goto :unsupportedWindowsVersion
if "%VERSION%" == "2.10" goto :unsupportedWindowsVersion
if "%VERSION%" == "2.11" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.00" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.10" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.1" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.11" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.2" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.5" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.50" goto :unsupportedWindowsVersion
if "%VERSION%" == "3.51" goto :unsupportedWindowsVersion
if "%VERSION%" == "4.00" goto :unsupportedWindowsVersion
if "%VERSION%" == "4.03" goto :unsupportedWindowsVersion
if "%VERSION%" == "4.0" goto :unsupportedWindowsVersion
if "%VERSION%" == "4.10" goto :unsupportedWindowsVersion
if "%VERSION%" == "4.90" goto :unsupportedWindowsVersion
REM only the versions below were tested for OS version identification (and the ones supported by this script).
if "%VERSION%" == "5.00" goto :unsupportedWindowsVersion
if "%VERSION%" == "5.0" goto :unsupportedWindowsVersion
if "%VERSION%" == "5.1" goto :unsupportedWindowsVersion
if "%VERSION%" == "5.2" goto :unsupportedWindowsVersion

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params= %*
    echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params:"=""%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"

echo Warning! This script comes without warranty. Use at your own risk.
echo You must always backup your registry before making any changes.
set /p continue="Do you want to continue? Y/N "
echo.
if /I NOT %continue% == Y goto :Exit

REM Query for TCMPath
%systemroot%\system32\Reg.exe Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT set /P TCMPath_noParams="Path to TOTALCMD.EXE "
if %OS%==64BIT set /P TCMPath_noParams="Path to TOTALCMD.EXE or TOTALCMD64.EXE: "

REM Add parameters to TCM.
REM /O opens in the current running TCM (opens one if none is running). /T opens in a new tab (depends on /O). /S opens it in the foreground.
set TCMPath="%TCMPath_noParams% /O /T /S \"%%1\"

goto :Menu

:Menu
echo.
echo Menu:
echo 0: Exit.
echo 1: Replace Windows Explorer with Total Commander.
echo 2: Undo 1.
echo 3: Add option to open with Total Commander to Windows Explorer context menu.
echo 4: Undo 3.
echo 5: Associate the Recycle Bin to open with Total Commander.
echo 6: Undo 5.
echo 7: Add Total Commander as an AutoPlay option.
set /p option="Select option: "
echo.
if /I %option% == 0 goto :Exit
if /I %option% == 1 goto :ReplaceExplorerTCM
if /I %option% == 2 goto :ReplaceExplorerTCM_undo
if /I %option% == 3 goto :OpenWithTCM
if /I %option% == 4 goto :OpenWithTCM_undo
if /I %option% == 5 goto :RecycleBin
if /I %option% == 6 goto :RecycleBin_undo
if /I %option% == 7 goto :Autoplay
goto :Exit

:ReplaceExplorerTCM
%systemroot%\system32\Reg.exe add "HKCR\Drive\shell" /ve /t REG_SZ /d "open" /f
%systemroot%\system32\Reg.exe add "HKCR\Drive\shell\open\command" /ve /t REG_SZ /d %TCMPath% /f
%systemroot%\system32\Reg.exe add "HKCR\Directory\shell" /ve /t REG_SZ /d "open" /f
%systemroot%\system32\Reg.exe add "HKCR\Directory\shell\open\command" /ve /t REG_SZ /d %TCMPath% /f
%systemroot%\system32\Reg.exe add "HKCR\Folder\shell\Total_Commander" /ve /t REG_SZ /d "Total Commander" /f
%systemroot%\system32\Reg.exe add "HKCR\Folder\shell\Total_Commander\command" /ve /t REG_SZ /d %TCMPath% \"%%1\"" /f

goto :Menu

:ReplaceExplorerTCM_undo
%systemroot%\system32\Reg.exe add "HKCR\Drive\shell" /ve /t REG_SZ /d "none" /f
%systemroot%\system32\Reg.exe delete "HKCR\Drive\shell\open" /f
%systemroot%\system32\Reg.exe add "HKCR\Directory\shell" /ve /t REG_SZ /d "none" /f
%systemroot%\system32\Reg.exe delete "HKCR\Directory\shell\open" /f
%systemroot%\system32\Reg.exe delete "HKCR\Directory\shell\open\command" /f
%systemroot%\system32\Reg.exe add "HKCR\Folder\shell" /ve /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe delete "HKCR\Folder\shell\Total_Commander" /f
goto :Menu

:OpenWithTCM
%systemroot%\system32\Reg.exe add "HKCR\Folder\shell\Total_Commander" /ve /t REG_SZ /d "Total Commander" /f
%systemroot%\system32\Reg.exe add "HKCR\Folder\shell\Total_Commander\command" /ve /t REG_SZ /d %TCMPath% /f
goto :Menu

:OpenWithTCM_undo
%systemroot%\system32\Reg.exe delete "HKCR\Folder\shell\Total_Commander" /f
goto :Menu

:RecycleBin
%systemroot%\system32\Reg.exe add "HKCR\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\shell\open\command" /ve /t REG_SZ /d %TCMPath% \"C:\RECYCLER\S-1-5-21-606747145-162531612-682003330-1003\\\"" /f
goto :Menu

:RecycleBin_undo
%systemroot%\system32\Reg.exe delete "HKCR\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\shell\open" /f
goto :Menu

:Autoplay
echo Warning! There is no undo script! Back up your registry!
set /p continue="Do you want to continue? Y/N "
if /I NOT %continue% == Y goto :Menu
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\AutorunINFLegacyArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\IomegaHipZipArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\MixedContentOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\PlayCDAudioOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\PlayDVDMovieOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\PlayMusicFilesOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\PlayVideoFilesOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlers\ShowPicturesOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\explorer\AutoplayHandlers\EventHandlers\UnknownContentOnArrival" /v "TOTALCMD_AUTOPLAY" /t REG_SZ /d "" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\Handlers\TOTALCMD_AUTOPLAY" /v "Action" /t REG_SZ /d "Open folder to view files" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\Handlers\TOTALCMD_AUTOPLAY" /v "DefaultIcon" /t REG_SZ /d "%TCMPath_noParams%,0" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\Handlers\TOTALCMD_AUTOPLAY" /v "InvokeProgID" /t REG_SZ /d "TOTALCMD_AUTOPLAY\AutoPlay" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\Handlers\TOTALCMD_AUTOPLAY" /v "InvokeVerb" /t REG_SZ /d "open" /f
%systemroot%\system32\Reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\Handlers\TOTALCMD_AUTOPLAY" /v "Provider" /t REG_SZ /d "Total Commander" /f
%systemroot%\system32\Reg.exe add "HKCR\TOTALCMD_AUTOPLAY\AutoPlay\shell\Open\command" /ve /t REG_SZ /d %TCMPath% /f
goto :Menu

:unsupportedWindowsVersion
echo Sorry. Windows version %VERSION% is not supported.
pause
goto :Exit

:Exit
Exit